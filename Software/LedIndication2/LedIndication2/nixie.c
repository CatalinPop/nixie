#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include "macros.h"

/************************************************************************/
/* Defines                                                              */
/************************************************************************/

#define OCR_1MILLISECOND	124
#define KEYPORT	PINC				// port for keys
#define KEYOFFSET 4					
#define KEYS 2						// number of keys
#define IMAX 100  					// max integrator value
#define SCR_MAX 2					// max screen number

#define HourAddress 0x00
#define DayAddress 0x01

#define DATA 5
#define SHIFT 7
#define LOAD 6

/************************************************************************/
/* Function headers                                                     */
/************************************************************************/

void tim1ms();
void tim2ms();
void tim5ms();
void tim10ms();
void tim20ms();
void tim100ms();
void tim1s();
void tim1s_async();
void display();

void ST_Start();
void ST_SetTime();
void ST_SetDay();
void ST_Run_Day();
void ST_Run_Time();

/************************************************************************/
/* Fields                                                               */
/************************************************************************/

uint8_t first_scan = 1;

/*
 *	States
 */

typedef enum{
	Start = 0,
	SetTime,
	SetDay,
	Run_Day,
	Run_Time
	} State_Type;

void (*state_table[])() = 
	{
		ST_Start,
		ST_SetTime,
		ST_SetDay,
		ST_Run_Day,
		ST_Run_Time
	};

State_Type curr_state;

/*
 *	Time
 */
int8_t sec=10, min=10, hour=10;
long day;

/*
 *	Display
 */
uint8_t distance[3];

/*
 *	Debounce
 */
uint8_t b0_old, b1_old, b2_old, b3_old;
uint8_t b0_new, b1_new, b2_new, b3_new;

uint8_t keys_in[KEYS];
uint8_t integrator[KEYS];

uint8_t btn[KEYS];
uint8_t btn_old[KEYS];
uint8_t btn_up[KEYS];
uint8_t btn_down[KEYS];

uint8_t btn_long[KEYS];
uint8_t btn_long_old[KEYS];
uint8_t btn_long_up[KEYS];

/************************************************************************/
/* Timers                                                               */
/************************************************************************/

uint8_t clockPause = 0;

uint16_t tim_disp_head;
uint16_t tim_b_on[KEYS];

volatile uint16_t ms = 0;
SIGNAL(TIMER0_COMP_vect)
{
	ms++;
	tim1ms();
	if(ms==0) return;
	
	if(ms%2==0)
	{
		tim2ms();
	}
	if(ms%3==0)
	{
		tim3ms();
	}
	if(ms%5==0)
	{
		tim5ms();
	}
	if(ms%10==0)
	{
		tim10ms();
	}
	if(ms%20==0)
	{
		tim20ms();
	}
	if(ms%100==0)
	{
		tim100ms();
	}
	if(ms==1000)
	{
		tim1s();
		ms = 0;
	}
}

SIGNAL(TIMER2_OVF_vect)
{
	tim1s_async();
}

void tim1ms()
{
	
}

void tim2ms()
{
	
}

void tim3ms()
{
	
}

void tim5ms()
{
	
}

void tim10ms()
{
	
}

void tim20ms()
{
	
}

void tim100ms()
{
	int i;
	
	if(tim_disp_head > 0) tim_disp_head--;
	
	for (i = 0; i < KEYS; i++)
	{
		if(btn[i]) tim_b_on[i]++;
	}
}

void tim1s()
{			
	
}

uint8_t sec_dot = 0;

void tim1s_async()
{			
	if(clockPause == 0)
	sec++;
	if(sec >= 60)
	{
		min++;
		sec = 0;
	}
	if(min >= 60)
	{
		hour++;
		min = 0;
	}
	if(hour >= 24)
	{
		day++;
		hour = 0;
	}
	
	
	display_refresh();
}

/************************************************************************/
/* Methods                                                              */
/************************************************************************/
				   // 0   1   2   3   4   5   6   7   8   9   DP
uint8_t D13msk[10] = { 5,  4,  3,  2, 12, 11, 10,  8,  7,  6,   1};
uint8_t D24msk[10] = { 6,  4,  3,  2, 12, 11, 10,  9,  8,  7,   1};

char disp[3] = "2356";
//DIGIT
uint8_t d1=2;
uint8_t d2=0;
uint8_t d3=3;
uint8_t d4=5;
//DISTANCE
uint8_t distance[3];

void bangx(uint8_t data, uint8_t cnt)
{
	bang(1);
	cnt--;
	
	while(cnt)
	{			
		bang(0);			
		cnt--;
	}
	
}

void bang(uint8_t data)
{
	if (data)
	{
		bit_set(PORTB, (1<<DATA));		
	}
	
	bit_set(PORTB, (1<<SHIFT));
	//delay?
	bit_clear(PORTB, (1<<SHIFT));
		
	bit_clear(PORTB, (1<<DATA));	
	
}

void load()
{
	bit_set(PORTB, (1<<LOAD));
	//delay?
	bit_clear(PORTB, (1<<LOAD));
}

void get_dist()
{
	distance[3] = D24msk[d4]+(3*12) - D13msk[d3]+(2*12);
	distance[2] = D13msk[d3]+(2*12) - D24msk[d3]+(1*12);
	distance[1] = D24msk[d2]+(1*12) - D13msk[d1]+(0*12);
	distance[0] = D13msk[d1]+(0*12);
}

void push(uint8_t digit)
{
	//if(digit == 12)
		//bangx()
}

void display_refresh()
{
	d1 = hour/10;
	d2 = hour%10;
	d3 = min/10;
	d4 = min%10;
			
	get_dist();			
	
	bangx(1, distance[0]);
	bangx(1, distance[1]);
	bangx(1, distance[2]);
	bangx(1, distance[3]);
	load();	
		
	load();
}

void init_timer()
{
	TCCR0 = 0;
	
	// CTC
	bit_clear(TCCR0, BIT(WGM00));
	bit_set(TCCR0, BIT(WGM01));
	
	// Clock select 64 prescaler
	bit_set(TCCR0, BIT(CS00));
	bit_set(TCCR0, BIT(CS01));
	
	//	Clear the timer/counter register 0
	TCNT0 = 0;
	
	bit_set(TIMSK, BIT(OCIE0));
	bit_set(TIMSK, BIT(TOIE0));
	
	//	Set the top value of the counter by setting the
	//	OCR register to the predetermined value
	OCR0  = OCR_1MILLISECOND;
}

void init_timer_async()
{
	/*
	 *	TCCR
	 */
	TCCR2 = 0;	
	// Clock select (128 prescaler)
	bit_set(TCCR2, (1<<CS22));
	bit_clear(TCCR2, (1<<CS21));
	bit_set(TCCR2, (1<<CS20));
	
	/*
	 *	ASSR
	 */
	bit_set(ASSR, (1<<AS2));
	
	/*
	 *	TIMKS
	 */	
	bit_set(TIMSK, BIT(TOIE2));
}

void init_io()
{	
	bit_set(DDRA, 1<<4);		//output
	
	bit_set(DDRB, 1<<4);		//output
	bit_set(DDRB, 1<<5);		//output
	bit_set(DDRB, 1<<6);		//output
	bit_set(DDRB, 1<<7);		//output
	
	bit_clear(DDRC, 1<<5);		//input
	bit_set(PORTC, 1<<5);		//pull-up
	bit_clear(DDRC, 1<<4);		//input
	bit_set(PORTC, 1<<4);		//pull-up
}

void debouncing()
{
	uint8_t i, j, key;
	for(i = KEYOFFSET; i < (KEYOFFSET + KEYS); i++)
	{
		j = i - KEYOFFSET;
		
		key = bit_get(KEYPORT, 1<<i);
		if(key != 0)
		{
			if(integrator[j] > 0)
			integrator[j]--;
		}
		else if (integrator[j] < IMAX) integrator[j]++;
		
		if (integrator[j] == 0)
		btn[j] = 0;
		else if (integrator[j] >= IMAX)
		{
			btn[j] = 1;
			integrator[j] = IMAX;  /* defensive code if integrator got corrupted */
		}
	}
}

void IO()
{
	int i;
	debouncing();
	
	for (i = 0; i < KEYS; i++)
	{
		/*
		 *	Pulse
		 */
		if(btn[i]!=btn_old[i])
		{
			if(btn[i] == 0) 
			{
				btn_up[i] = 1;
				tim_b_on[i] = 0;
			}
			else
			{
				btn_down[i] = 1;
			}
			
			btn_old[i] = btn[i];
		}
		else
		{
			btn_up[i] = 0;
			btn_down[i] = 0;	
		}	
		
		/*
		 *	Long press
		 */		
		if(btn[i])
		{
			if(tim_b_on[i] >= 10)
			{
				btn_long[i] = 1;
				if(btn_long_old[i] == 0)
				{
					btn_long_up[i] = 1;
					btn_long_old[i] = 1;
				}
				else btn_long_up[i] = 0;
			}
		}
		else btn_long_old[i] = 0;
	}	
}

void inc_sec(int i)
{
	sec += i;
	if(sec >= 60) sec -= 60;
	if(sec < 0) sec += 60;
}

void inc_min(int i)
{
	min += i;
	if(min >= 60) min -= 60;
	if(min < 0) min += 60;
}

void inc_h(int i)
{
	hour += i;
	if(hour >= 24) hour -= 24;
	if(hour < 0) hour += 24;
}

void inc_day(int i)
{
	day += i;
	if(day < 0) day = 0;
	if(day > 9999) day = 9999;
}


/*
 *	States
 */

void ST_Start()
{

}

void ST_SetTime()
{
	
}

void ST_SetDay()
{
	
}

void ST_Run_Day()
{
	
}

void ST_Run_Time()
{
	
}

/************************************************************************/
/* MAIN                                                                 */
/************************************************************************/

void fisrttest()
{
	// 0____
	if(bit_get(PINC, 1<<5))
	{
		bit_clear(PORTB, (1<<5));
	}
	else
	{
		bit_set(PORTB, (1<<5));
	}
	
	// ____0
	if(bit_get(PINC, 1<<4))
	{
		bit_set(PORTB, (1<<6));
		bit_clear(PORTB, (1<<7));
	}
	else
	{
		bit_clear(PORTB, (1<<6));
		bit_set(PORTB, (1<<7));
	}
}

void clear()
{
	bangx(0, 50);
	load();
}

int main(void)
{	
	//scr_nr = 0;		
	init_io();
	init_timer();
	init_timer_async();
	
	sei();			
	
	clear();	
	
	while(1) 
	{				
		IO();				
		
		//state_table[curr_state]();		
		
		//// 0____
		//if(bit_get(PINC, 1<<5))
		//{
			//bit_clear(PORTA, (1<<4));
		//}
		//else
		//{
			//bit_set(PORTA, (1<<4));
		//}	
		
									
		if(btn_up[0])
		{
			min++;
		}		
		
		if(btn_up[1])
		{
			hour++;
		}
	}
}